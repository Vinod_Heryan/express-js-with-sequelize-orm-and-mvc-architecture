const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const routerRoutes = require('./config/routes/router');

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.use('/router', routerRoutes);

//message error
app.use((req, res, next) => {
    const error = new Error('URL Tidak Ditemukan');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});
 

module.exports = app;
 
// // listen on port
// app.listen(3000, () => console.log('Server Running at http://localhost:3000'));


// // basic route
// app.get('/', (req, res) => {
//     res.send('Hello World');
// });

// app.use((req, res, next) => {
//     res.status(200).json({
//         message:"Server Running at http://localhost:3000",
//     });
// });