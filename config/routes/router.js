const express = require('express');
const router = express.Router();
const controller = require('../controller/index');


router.get('/', controller.dataController.getAll);

router.get('/search', controller.dataController.search);

router.get('/:id', controller.dataController.getById);

router.post('/', controller.dataController.post);

router.put('/', controller.dataController.put);

router.delete('/:id', controller.dataController.delete);


module.exports = router;