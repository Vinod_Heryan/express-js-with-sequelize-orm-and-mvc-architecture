const model = require('../model/index');
const controller = {};
const {Op} = require('sequelize')

controller.getAll = async function(req,res) {
    try{
        let data = await model.data.findAll({
            //get data tertentu menggunakan attributes:. kalau menggunakan alias [['name','namaApa']]
            attributes: ['name'],
            order: [['id','asc']],
            limit:5
        });
        if (data.length > 0) {
            res.status(200).json({
                message: 'Get All Data',
                data: data
            })
        }else{
            res.status(200).json({
                message: 'Data Kosong',
                data: []
            })
        }
    }catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

//jika ingin menggunakan where dengan and, or dan between,. menggunakan op
// where:{
//     [Op.and]: [
//         {nama: 'bambang'},
//         {jk: 'laki-laki'}
//     ]
// }
// where:{
//     name: {
//         [Op.and]: ['bambang','bembeng']
//     }
// }

// where:{
//     [Op.or]: [
//         {nama: 'bambang'},
//         {jk: 'laki-laki'}
//     ]
// }
// where:{
//     name: {
//         [Op.or]: ['bambang','bembeng']
//     }
// }

// where:{
//     umur: {
//         [Op.between]: ['10','20']
//     }
// }

controller.search = async function(req,res) {
    const search = req.query.search;
    try{
        let data = await model.data.findAll({
            where:{
                name:{
                    [Op.like]: '%'+search+'%'
                }
            }
        });
        if (data.length > 0) {
            res.status(200).json({
                message: 'Get Data Berhasil',
                data: data
            })
        }else{
            res.status(200).json({
                message: 'Data Kosong',
                data: []
            })
        }
    }catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

controller.getById = async function(req,res) {
    try{
        let data = await model.data.findAll({
            where:{
                id: req.params.id
            }
        });
        if (data.length > 0) {
            res.status(200).json({
                message: 'Get Data By Id',
                data: data
            })
        }else{
            res.status(200).json({
                message: 'Data Kosong',
                data: []
            })
        }
    }catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

controller.post = async function(req,res) {
    try{
        let data = await model.data.create({
           name: req.body.name
        });
        res.status(201).json({
            message: 'Tambah Data Berhasil',
            data: data
        })
    }catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

controller.put = async function(req,res) {
    try{
        await model.data.update({
           name: req.body.name
        },{
            where:{
                id: req.body.id
            }
        });

        let data = await model.data.findAll({
            where:{
                id: req.body.id
            }
        });

        res.status(200).json({
            message: 'Data Berhasil Diupdate',
            data: data
        })
    }catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

controller.delete = async function(req,res) {
    try{
        await model.data.destroy({
            where:{
                id: req.params.id
            }
        });
        res.status(200).json({
            message: 'Data Berhasil Dihapus',
        })
    }catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

module.exports = controller;