const Sequelize = require('sequelize');
const db = require('../database/mysql');

// data = nama tabel
var data = db.define('data',
{
    name: Sequelize.STRING,
},{
    freezeTableName: true,
    timestamps: false
});

module.exports = data;